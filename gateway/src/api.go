package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func initApi() {
	// router
	r := mux.NewRouter()
	r.HandleFunc("/api/accounts", apiAccount).Methods("POST")

	// start server
	err := http.ListenAndServe(":7654", r)
	if err != nil {
		log.Printf("ERROR: fail init http server, %s", err.Error)
		os.Exit(1)
	}
}

func apiAccount(w http.ResponseWriter, r *http.Request) {
	// read body
	data, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	log.Printf("INFO: request account contract %s", string(data))

	// unmarshal json
	jsonMap := make(map[string]interface{})
	err := json.Unmarshal(data, &jsonMap)
	if err != nil {
		log.Printf("ERROR: fail to unmarshla json, %s", err.Error())
		response(w, "Invalid request json", 400)
		return
	}
	log.Printf("INFO: JSON map, %s", jsonMap)

	// OK response
	response(w, "OK", 200)
}

func response(w http.ResponseWriter, resp string, status int) {
	w.WriteHeader(status)
	w.Header().Set("Content-Type", "application/json")
	io.WriteString(w, string(resp))
}
